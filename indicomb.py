#!/usr/bin/env python3

"""
indicomb.py - combs through indico and builds a listing of your meetings for display as a webpage or importing to iCal
"""
from __future__ import print_function

# Original author: Will Buttinger (will@cern.ch)
# Current maintainer: Dan Guest (daniel.hay.guest@cern.ch)
# PLEASE DO NOT COPY AND HACK THIS SCRIPT
# Send feature requests to the above email address(es)


#from future import standard_library
#standard_library.install_aliases()
#from builtins import map
#from builtins import str
import hashlib
import hmac
try:
    import urllib.request, urllib.parse, urllib.error
except ImportError:
    import urllib
    import urllib2
    urllib.request = urllib2
    urllib.parse = urllib
    urllib.error = urllib
import time
import json

from datetime import datetime
from datetime import timedelta
import re
from collections import OrderedDict

def build_indico_request(path, params, api_key=None, secret_key=None, only_public=False, persistent=False):
    items = list(params.items()) if hasattr(params, 'items') else list(params)
    if api_key:
        items.append(('apikey', api_key))
    if only_public:
        items.append(('onlypublic', 'yes'))
    if secret_key:
        if not persistent:
            items.append(('timestamp', str(int(time.time()))))
        items = sorted(items, key=lambda x: x[0].lower())
        url = '%s?%s' % (path, urllib.parse.urlencode(items))
        signature = hmac.new(secret_key.encode('utf-8'), url.encode('utf-8'), hashlib.sha1).hexdigest()
        items.append(('signature', signature))
    if not items:
        return path
    return '%s?%s' % (path, urllib.parse.urlencode(items))

# ics calendar methods
# written by yuchen@cern.ch
prog_vcalendar = re.compile(r"(?P<header>.*?)(?P<body>BEGIN:VEVENT.*END:VEVENT)(?P<tail>.*)", re.DOTALL)
prog_vevent = re.compile(r"BEGIN:VEVENT.*?END:VEVENT", re.DOTALL)
prog_vuid = re.compile(r"UID:(.*)", re.MULTILINE)
def read_calendar(text):
    def newlined(text):
        '''Without this, you will get scumbagged by iCal2.0...'''
        return '\n'.join(text.splitlines()).strip()
    matched = prog_vcalendar.match(str(text))
    if matched:
        caldict = matched.groupdict()
        for event in prog_vevent.findall(caldict['body']):
            matched = prog_vuid.search(event)
            uid = ''
            if matched:
                uid = matched.group(1).strip()
            else:
                raise KeyError("Find no UID")
            en = {'header': [newlined(caldict['header'])], 'body': OrderedDict([(uid, [newlined(event)])]), 'tail': [newlined(caldict['tail'])]}
            yield en
    else:
        raise IOError("Read ics file fail!")

def merge_calendars(calendars, skip_duplicates = True, debug = False):
    isFirst = True
    Allcal = {'header': [], 'body': OrderedDict(), 'tail': []}
    for cal in calendars:
        if isFirst:
            Allcal['header'] = cal['header'][:]
            Allcal['tail'] = cal['tail'][:]
        for uid, entries in list(cal['body'].items()):
            for entry in entries:
                Allcal['body'].setdefault(uid, []).append(entry)
        isFirst = False
    if skip_duplicates:
        for uid, entries in list(Allcal['body'].items()):
            if len(entries) > 1 and debug:
                print("UID: {} has multiple versions. Update to the latest one.".format(uid))
            entries[:] = [entries[-1]]
    return Allcal

def request_calendars(events, api_key = None, secret_key = None, indico_site = None):
    for event in events:
        icalurl = "{}/{}".format(indico_site,build_indico_request("/export/event/{id}.ics".format(**event), {}, api_key, secret_key))
        respond =  urllib.request.urlopen(urllib.request.Request(icalurl)).read()
        for cal in read_calendar(respond):
            yield cal



#This is the main method
#combs through indico looking for meetings of interest
def indicomb(    output="",              #where to put output, e.g. "/afs/cern.ch/user/w/will/www/MyMeetings.html"
                 API_KEY='',             #you can get this from https://indico.cern.ch/user/api/ or specify in a .indico-secret-key file
                 SECRET_KEY='',          #you can get this from https://indico.cern.ch/user/api/ or specify in a .indico-secret-key file
                 categoryNumbers=[],     #which indico categories need to be combed, e.g. [490]
                 includeList=[],         #strings to look for in meeting titles, e.g. ["Analysis"]
                 excludeList=[],         #veto meetings that have these strings in title, e.g. ["CANCELLED"]
                 speakerList=[],         #if specified, will only show contributions from specific speakers (searches speaker name or affiliation)
                 startDate="2017-04-01", #what date to start combing from
                 endDate="+14d",         #what date to go up to (default is up to 2 weeks into future)
                 cacheDate="-14d",       #what date that we actually start combing from if a webpage already exists
                 headerHTML="",          #Any HTML you want to appear at the top of the page
                 prebodyHTML="",         #HTML appearing before the <body> (experts only)
                 indico_site="http://indico.cern.ch", #where indico is hosted that you want to scan"
                 calendarName="",        #generates ics file. Leave blank to use the output html filename to set name. Set to None to disable
                 noTopbar = False,       #If true, wont generate a top bar with links
                 pageTitle = "",         #If blank will use output filename without extension
                 debug=False,            #If true, displays debug info
                 regex=False) :          #If true, will interpret includeList as regex

    #need next three lines to fix an ascii encoding issue
    import sys
    try:
        reload(sys)
        sys.setdefaultencoding('utf-8')
    except NameError:
        # python3 
        # shouldn't need to fix
        pass

    if API_KEY=='' or SECRET_KEY=='':
        # try to load from secret file
        import os
        try:
            lines = open(os.path.expanduser('~/.indico-secret-key')).read()
            API_KEY, SECRET_KEY = lines.split()
        except:
            pass

    if API_KEY=='' or SECRET_KEY=='':
         print("For security reasons since 4/4/2019 you must go to " + indico_site + "/user/api/ and request a key, then pass the token to API_KEY and secret to SECRET_KEY arguments of the script or put them in a ~/.indico-secret-key file")
         sys.exit()

    PARAMS = {
        'from': startDate,
        'to': endDate,
        'pretty':"yes",
        'order':"start"
    }

    #record the start of today
    source_datetime = datetime.now()
    sod = datetime(
        year=source_datetime.year,
        month=source_datetime.month,
        day=source_datetime.day)


    cacheDatetime = sod - timedelta(days=int(re.findall(r'\d+',cacheDate)[0]))

    #check if output file exists and it includes a date comment in it that is earlier than the cache date ...
    lineCache = []
    import os
    if os.path.isfile(output):
        with open(output, "r") as ins:
            startCache=False
            endCache=False
            for line in ins:
                if (not startCache) and line.startswith("<!--"):
                    match=re.search(r'(\d+-\d+-\d+)',line)
                    if match and datetime.strptime(match.group(0),"%Y-%m-%d") <= cacheDatetime:
                        startCache = True
                        #move cacheDatetime to this date so that we definitely catch anything since that date
                        cacheDatetime = datetime.strptime(match.group(0),"%Y-%m-%d")
                if "<!-- ENDCACHE -->" in line:
                    endCache = True
                if startCache and not endCache:
                    lineCache.append(line)

    if len(lineCache) > 0:
        PARAMS['from'] = (cacheDatetime+timedelta(days=1)).strftime("%Y-%m-%d")
        print("Using cached results for events earlier than %s  (delete the output file to do a full recomb)" % PARAMS['from'])

    allEvents = []
    hasFuture=False

    #ask indico for all events (meetings) in categories
    for categoryNumber in categoryNumbers:
        PATH = '/export/categ/%d.json' % categoryNumber
        fullurl = indico_site + build_indico_request(PATH, PARAMS, API_KEY, SECRET_KEY)
        print("indiCombing category %d ... " % categoryNumber)
        if debug: print(fullurl)
        try:
            r = urllib.request.Request(fullurl)
            reply = json.loads(urllib.request.urlopen(r).read().decode('utf-8'))
        except urllib.error.HTTPError as e:
            print("An HTTPError occurred, exiting: {}".format(str(e)))
            sys.exit(1)

        #filter events as they come in ...
        for event in reversed(reply[u'results']):
            # must have at least one occurance of includeList
            if not regex:
                if not any(patt in event[u'title'] for patt in includeList):
                    # or the includeList can be empty
                    if includeList:
                        continue
            else:
                # must have at least one regex match
                if not any(re.match(patt, event[u'title']) is not None for patt in includeList):
                    continue
            if any(patt in event[u'title'] for patt in excludeList): continue #must not have any occurance of excludeList

            myEvent = dict() #save the required info of the event
            myEvent["id"] = event[u'id']
            myEvent["url"] = event[u'url']
            myEvent["title"] = event[u'title'].encode('ascii', 'ignore')
            myEvent["title"] = myEvent["title"].replace(b'&', b'&amp;')
            myEvent["date"] = event[u'startDate'][u'date']

            if u'note' in list(event.keys()) and u'url' in event[u'note']:
                myEvent["minutes_url"] = event[u'note'][u'url']

            #flag if event is in the past
            myEvent["inThePast"] = (datetime.strptime(myEvent['date'],"%Y-%m-%d") < sod)
            if not myEvent["inThePast"]:
                myEvent["time"] = event[u'startDate'][u'time']  #will display time for future events
                if u'roomFullname' in list(event.keys()): myEvent["room"] = event[u'roomFullname']
                hasFuture=True
            allEvents += [myEvent]

#            if debug: datetime.strptime(event['date']+" " + myEvent.get("time","00:00:00"),"%Y-%m-%d %H:%M:%S"))
    #sort events by date
    allEvents.sort(key=lambda event:datetime.strptime(event['date']+" " + event.get("time","00:00:00"),"%Y-%m-%d %H:%M:%S"))

    import os
    cal_file = os.path.splitext(output)[0] + ".ics"
    if calendarName != "" and calendarName is not None: cal_file = calendarName

    # if requested, make all the requests for calendar files from indico, and combine
    if calendarName is not None:
        calendars = []
        if os.path.exists(cal_file):
            with open(cal_file) as of:
                calendars.extend(read_calendar(of.read()))
        calendars.extend(request_calendars(allEvents, API_KEY, SECRET_KEY, indico_site))
        cal = merge_calendars(calendars,True,debug)
        with open(cal_file, 'w') as ics:
            body = []
            list(map(body.extend, list(cal['body'].values())))
            ics.writelines('\n'.join(cal['header']+body+cal['tail']))

    if pageTitle == "":
        pageTitle = os.path.splitext(os.path.basename(output))[0]


    f = open(output, 'w')
    f.write('<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">')
    f.write("""<html><head><title>%s</title>%s</head><body>""" % (pageTitle,prebodyHTML))

    if not noTopbar:
        f.write("""<table class="topbar" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="white"><tr><td class="ics">""")
        if calendarName is not None:
            f.write("""<a href="#" onclick="javascript:location.href=document.location.href.replace(/[^/]*$/, '').replace(/[^:]*/,'webcal')+'{0}';">Subscribe to Calendar</a> | <a href="#" onclick="location.href='https://calendar.google.com/calendar/r?cid='+document.location.href.replace(/[^/]*$/, '')+'{0}';">Add to Google Calendar</a>""".format(os.path.relpath(cal_file, os.path.dirname(output))))
        f.write("""</td><td class="help" align="right" valign="middle"><a href="https://gitlab.cern.ch/indicomb/indicomb">help<img src="https://gitlab.cern.ch/uploads/-/system/project/avatar/49048/Screen_Shot_2018-10-04_at_20.31.51.png" title="indicomb" width="40" align="middle" alt="indicomb logo"/></a></td></tr></table>\n""")

    f.write("""%s
<center><h5>(page last updated: %s)</h5></center>\n""" % (headerHTML,time.ctime()))

    #loop over the events found in request ...
    i=0
    intoPast=False
    for event in reversed(allEvents):
        i=i+1
        print(event['date'] + ":")

        inThePast = (datetime.strptime(event['date'],"%Y-%m-%d") < sod)
        #query for contributions
        PATH = '/export/event/%d.json' % int(event['id'])
        PARAMS = { 'pretty':"yes",'detail':"subcontributions"}
        fullurl = indico_site + build_indico_request(PATH, PARAMS, API_KEY, SECRET_KEY)
        if debug: print(fullurl)
        r = urllib.request.Request(fullurl)
        event_r = json.loads(urllib.request.urlopen(r).read().decode('utf-8'))[u'results'][0]
        
        if len(speakerList)>0: 
            #need to see if event has a contribution matching the speaker list ...
            isOk=False
            for contrib in reversed(event_r[u'contributions']):
                if debug: print('DEBUG contrib', contrib)

                if len(contrib[u'folders'])==0 and inThePast==True and len(contrib[u'subContributions'])==0: continue #skip empty contributions (and no subcontributions), unless event is in the future
                for speaker in contrib[u'speakers']:
                    if any(patt in speaker[u'fullName'] for patt in speakerList) or any(patt in speaker[u'affiliation'] for patt in speakerList):
                        isOk = True; break
                if isOk: break
                #check subcontributions
                for subcontrib in contrib[u'subContributions']:
                    if inThePast and len(subcontrib[u'folders'])==0: continue
                    for speaker in subcontrib[u'speakers']:
                        if any(patt in speaker[u'fullName'] for patt in speakerList) or any(patt in speaker[u'affiliation'] for patt in speakerList):
                            isOk = True; break
                    if isOk: break

            if not isOk: continue #go to next event


        #next meeting gets highlighted in bold
        inThePast = event['inThePast']

        if intoPast==False and inThePast and i!=0:
            f.write('<hr width="100%"/><br/>\n')
            intoPast=True

        if intoPast: f.write("""<!-- %s -->\n""" % event['date']) #used to in caching mechanism

        nextMeeting=False
        if not inThePast and (event==allEvents[0] or allEvents[-(i+1)]['inThePast']):
            f.write("""<b>""");
            nextMeeting=True
        event['title'] =  event['title'].replace(b'&',b'&amp;').decode('utf-8')

        f.write("""<a href="%s">%s%s%s</a> - %s\n""" % (event['url'],event['date'],"" if inThePast else " - (%s)" % event['time'], "" if "room" not in list(event.keys()) else " - (room: %s)" % event['room'],event['title']))
        if "minutes_url" in list(event.keys()):
            f.write("- <a href='%s'>Minutes</a>\n" % event[u'minutes_url'])

        if nextMeeting: f.write("""</b>""")

        #write contributions and subcontributions
        f.write("<ul>\n");
        #Note - in some contributions the startDate is 'None' ... these shouldn't be interesting anyways, but we put them first with "0" time
        for contrib in sorted(event_r[u'contributions'],key=lambda x: x[u'startDate'][u'time'] if x[u'startDate'] else "0"):

            minContrib = False 
            if u'note' in contrib and u'url' in contrib[u'note']:
                minContrib = True

            if len(contrib[u'folders'])==0 and inThePast==True and len(contrib[u'subContributions']) == 0 and minContrib==False: 
                continue #skip empty contributions (and no subcontributions), unless event is in the future

            isOk=True
            if len(speakerList)>0:
                isOk=False
                for speaker in contrib[u'speakers']:
                    if any(patt in speaker[u'fullName'] for patt in speakerList) or any(patt in speaker[u'affiliation'] for patt in speakerList):
                        isOk = True; break
                if not isOk:
                    #check subcontributions too ...
                    for subcontrib in contrib[u'subContributions']:
                        for speaker in subcontrib[u'speakers']:
                            if any(patt in speaker[u'fullName'] for patt in speakerList) or any(patt in speaker[u'affiliation'] for patt in speakerList):
                                isOk = True; break
                        if isOk: break


            ctitle = contrib[u'title'].encode('ascii', 'ignore')
            ctitle = ctitle.replace(b'&',b'&amp;').decode('utf-8')
            print("  " + ctitle)
            if not isOk: continue #go to next contrib

            f.write("""<li><a href="%s">%s</a>""" % (contrib[u'url'],ctitle))
            if minContrib:
                f.write("- <a href='%s'>Minutes</a>\n" % contrib[u'note'][u'url'])


            for speaker in contrib[u'speakers']:
                f.write(""" - %s %s""" % (speaker[u'first_name'],speaker[u'last_name']))

            def sizeof_fmt(num, suffix="B"):
                for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
                    if abs(num) < 1024.0:
                        return "{:3.1f}{}{}".format(num,unit,suffix)
                    num /= 1024.0
                return "{:.1f}Y{}".format(num,suffix)

            for fold in contrib[u'folders']:
                if u'attachments' in fold.keys():
                    f.write("<ul>\n")
                    for attach in fold[u'attachments']:
                        if u'size' in attach.keys():
                            f.write("""<li><a href="%s">%s</a> (%s)""" % (attach[u'download_url'], attach[u'title'], sizeof_fmt(attach[u'size'])))
                        else:
                            f.write("""<li><a href="%s">%s</a> (size unknown)""" % (attach[u'download_url'], attach[u'title']))
                        if debug: print('DEBUG attach ', attach[u'download_url'])
                    f.write("</ul>\n")

            wroteSubContrib=False

            for subcontrib in contrib[u'subContributions']:
                if inThePast and len(subcontrib[u'folders'])==0: continue
                sctitle = subcontrib[u'title'].encode('ascii', 'ignore')
                sctitle = sctitle.replace(b'&', b'&amp;').decode('utf-8')
                print("    " + sctitle)
                if len(speakerList)>0:
                    isOk = False
                    for speaker in subcontrib[u'speakers']:
                        if any(patt in speaker[u'fullName'] for patt in speakerList) or any(patt in speaker[u'affiliation'] for patt in speakerList):
                            isOk = True; break
                    if not isOk: continue #go to next contrib

                if wroteSubContrib==False:
                    wroteSubContrib=True
                    f.write("""<ul>\n""")
                f.write("""<li><a href="%ssubcontributions/%s">%s</a>"""  % (contrib[u'url'],subcontrib[u'db_id'],sctitle))
                for speaker in subcontrib[u'speakers']:
                    f.write(""" - %s %s""" % (speaker[u'first_name'],speaker[u'last_name']))
                f.write("""</li>\n""")

            if wroteSubContrib: f.write("""</ul>\n""") #closes subcontrib bullet points
            f.write("""</li>\n""");

        f.write("</ul>\n")

    for line in lineCache:
        f.write(line)

    f.write("""<!-- ENDCACHE -->\n""");

    if calendarName is not None:
        f.write('<br/><center><font size="2pt"><a href="' + os.path.relpath(cal_file, os.path.dirname(output)) + '">Calendar export</a></font></center>\n')

    f.write("""<br/><center><font size="2pt">Page generated with <a href="https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Indicomb">indicomb</a> <a href="https://gitlab.cern.ch/indicomb/indicomb">(gitlab)</a></font></center><br/></body></html>\n""")
    f.close()

if __name__ == '__main__':
    
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('--output',   default="",    help="where to put output, e.g. '/afs/cern.ch/user/w/will/www/MyMeetings.html'",required=True)
    parser.add_argument('--API_KEY',   default="",   help="you can get this from https://indico.cern.ch/user/api/ or specify in a .indico-secret-key file")
    parser.add_argument('--SECRET_KEY',default="",   help="you can get this from https://indico.cern.ch/user/api/ or specify in a .indico-secret-key file")
    parser.add_argument('--categoryNumbers',type=int,nargs='+',help="which indico categories need to be combed, e.g. [490]",required=True)
    parser.add_argument('--includeList',nargs='*',default=[],help="strings to look for in meeting titles, e.g. Analysis")
    parser.add_argument('--excludeList',nargs='*',default=[],help="veto meetings that have these strings in title, e.g. CANCELLED")
    parser.add_argument('--speakerList',nargs='*',default=[],help="if specified, will only show contributions from specific speakers (searches speaker name or affiliation)")
    parser.add_argument('--startDate',default="2017-04-01",help="what date to start combing from. defaults to 2017-04-01 (indicombs birthday!)")
    parser.add_argument('--endDate',default="+14d",help="what date to go up to. default is up to 2 weeks into future")
    parser.add_argument('--cacheDate',default="-14d",help="what date that we actually start combing from if a webpage already exists")
    parser.add_argument('--headerHTML',default="", help="Any HTML you want to appear at the top of the page")
    parser.add_argument('--pageTitle',default="",help="Specify a title for the page - if blank will use output's filename without extension")
    parser.add_argument('--prebodyHTML',default="",help="HTML appearing before the <body> (experts only)")
    parser.add_argument('--indico_site',default="http://indico.cern.ch", help="where indico is hosted that you want to scan, defaults to indico.cern.ch")
    parser.add_argument('--calendarName',default="",help="Name of ics calendar file alongside html file - leave blank will set from output html name")
    parser.add_argument('--noTopbar',action='store_true',help="If true, wont generate the top bar with links")
    parser.add_argument('--debug',action='store_true',help="If true, displays debug info")
    parser.add_argument('--regex',action='store_true',help="Add this option to interpret includeList as regex patterns")

    args = parser.parse_args()

    if args.debug:
        print("Arguments passed: {}".format(vars(args)))

    indicomb( **vars(args)  ) 
    
